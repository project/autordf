<?php

class Autordf {
  protected $min_word_length;
  protected $noisewords = array();
  protected $bundle;
  protected $threshold;
  protected $vocab = array();
  
  function __construct($text, $bundle = NULL) {
    $this->text = $text;
    $this->bundle = $bundle;
    $this->min_word_length = variable_get('autordf_minlength', 3);
    $this->threshold = variable_get('autordf_tags_threshold', 0.02);
    $this->noisewords = array_map('trim', explode(',', variable_get('autordf_ignorelist', AUTORDF_IGNORE_LIST_DEFAULT)));
  }
  
  public function get_vocabularies() {
  	return $this->vocab;  
  }
  
  public function set_vocabularies($vocab) {
  	$this->vocab = $vocab;
  }
  
  public function keywordexist($keyword) {
    $vocab = $this->vocab;
    if ($vocab) {
      foreach($vocab as $key => $values) {
        if ($values && in_array($keyword, array_keys($values))) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }  
  
  public function isnoiseword($word) {
    $word = drupal_strtolower($word);
  	return in_array($word, $this->noisewords)? TRUE: FALSE;
  }
  
  function formatcontent($content) {
    if ($content) {
      // Decode entities to UTF-8
      $content = decode_entities($content);
      // Chinese, Japanese, Korean handling
      if (variable_get('overlap_cjk', TRUE)) {
        $content = preg_replace_callback('/[' . PREG_CLASS_CJK . ']+/u', 'search_expand_cjk', $content);
      }

      // ensure capitals next to full stops are decapitalised but only if the word is single e.g
      // change ". The world" to ". the" but not ". United States"
      // $content = preg_replace("/(\.[”’\"]?\s*[A-Z][a-z]+\s[a-z])/e","strtolower('$1')",$content);
      
      // remove plurals
      $content = preg_replace("/(\w)([‘'’]s )/i","$1 ",$content);

      // replace new lines with a full stop so we don't get cases of two unrelated strings being matched
      $content = preg_replace("/\r\n/",". ",$content);
            
      // remove excess space
      $content = preg_replace("/\s{2,}/"," ",$content);      
    }
    return $content;
  }

/**
 * Split a text separated by space into array of words
 */   
  function indexsplit($content) {
    $words = explode(' ', $content);
    array_walk($words, '_search_index_truncate');
    return $words;
  } 

/**
 * Untidy function takes full content as input and splits it into array pieces 
 * with score based on tag associated with it 
 */  
  function parsecontent($text) {
    global $base_url;

    // Multipliers for scores of words inside certain HTML tags
    $tags = variable_get('autordf_tag_weights', array(
      'h1'     => 1.6,
      'a'      => 1.3,
      'strong' => 1.3,
      'b'      => 1.3,
      'h2'     => 1.2,
      'h3'     => 1.2,
      'h4'     => 1.2,
      'h5'     => 1.2,
      'h6'     => 1.2,
      'u'      => 1.2,
      'i'      => 1.2,
      'em'     => 1.2,
    ));
  
    // Strip off all ignored tags to speed up processing, but insert space before/after
    // them to keep word boundaries.
    $text = str_replace(array('<', '>'), array(' <', '> '), $text);
    $text = strip_tags($text, '<' . implode('><', array_keys($tags)) . '>');
  
    //  Split HTML tags from plain text.
    $split = preg_split('/\s*<([^>]+?)>\s*/', $text, -1, PREG_SPLIT_DELIM_CAPTURE);

    $tag = FALSE; // Odd/even counter. Tag or no tag.
    $score = 1; // Starting score per word
    $tagstack = array(); // Stack with open tags
  
    foreach ($split as $value) {
      if ($tag) {
        // Increase or decrease score per word based on tag
        list($tagname) = explode(' ', $value, 2);
        $tagname = drupal_strtolower($tagname);
        // Closing or opening tag?
        if ($tagname[0] == '/') {
          $tagname = substr($tagname, 1);
          // If we encounter unexpected tags, reset score to avoid incorrect boosting.
          if (!count($tagstack) || $tagstack[0] != $tagname) {
            $tagstack = array();
            $score = 1;
          }
          else {
            // Remove from tag stack and decrement score
            $score = max(1, $score - $tags[array_shift($tagstack)]);
          }
        }
        else {
          if (isset($tagstack[0]) && $tagstack[0] == $tagname) {
            // None of the tags we look for make sense when nested identically.
            // If they are, it's probably broken HTML.
            $tagstack = array();
            $score = 1;
          }
          else {
            // Add to open tag stack and increment score
            array_unshift($tagstack, $tagname);
            $score += $tags[$tagname];
          }
        }
      }
      else {
        // Note: use of PREG_SPLIT_DELIM_CAPTURE above will introduce empty values
        if ($value != '') {
          $text_split[] = array('split' => $this->formatcontent($value), 'score' => $score);
        }
      }
      $tag = !$tag;
    }
    return $text_split;
  }
}

class AutordfTag extends Autordf {
    
  // PHP5 automatically calls constructors of base class

  public function categorize() {
    $split = $this->parsecontent($this->text);
    $vocab = array();
    foreach($split as $s) {
      $value = $s['split'];
      $score = $s['score'];
      $this->matchcountries($value, $vocab['Country'], $score);
      $this->matchacronyms($value, $vocab['Acronyms'], $score);
      $this->matchemails($value, $vocab['EmailAddress'], $score);
      $totalwords = $this->gettags($value, $vocab['tags'], $score);
    }
    $this->filter_tags($vocab['tags'], $totalwords);
    $this->set_vocabularies($vocab);
  }
  
  protected function gettags($content, &$vocab, $score = 1) {
    static $word_count = 0;
    
    $tags = &$vocab;
    $focus = 1; 
    $tagwords = 0;
    
    $content = drupal_strtolower($content);
    $content = preg_replace('/[' . PREG_CLASS_SEARCH_EXCLUDE . ']+/u', ' ', $content);
    
    $words = $this->indexsplit($content);
    $word_count += count($words); 
    
    foreach ($words as $word) {
      $num = is_numeric($word);
      
      // Only characters and no numbers
      if (!$num && drupal_strlen($word) >= $this->min_word_length) {
 
        if (!isset($tags[$word])) {
          $tags[$word] = 0;
        }
        $tags[$word] += $score * $focus;
        //  Focus is a decaying value in terms of the amount of unique words up to this point.
        //  From 100 words and more, it decays, to e.g. 0.5 at 500 words and 0.3 at 1000 words.
        $focus = min(1, .01 + 3.5 / (2 + count($tags) * .015));
      }
      $tagwords++;
      //  Too many words inside a single tag probably mean a tag was accidentally left open.
      if ($tagwords >= 15) {
        $score = 1;
      }
    }
    return $word_count;
  }
  
  protected function filter_tags(&$tags, $totalwords) {
    // Filter words from ignore list
    $noisewords = array_flip($this->noisewords);
    $tags = array_diff_key($tags, $noisewords);
    
    // Set a threshold score for tags
    $threshold = max(2.5, $this->threshold * $totalwords);
    foreach($tags as $tag => $score) {
      if($score < $threshold) {
        unset($tags[$tag]);       
      }
    }
  }
  
  
  /**
   * Checks whether a word is a roman numeral
   *
   * @param string $word
   * @return boolean
   */
  protected function isromannumeral($word) {
    if(preg_match("/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/",$word)) {
      return true;
    } 
    else {
      return false;
    }
  }
  
	/**
   * This function will count CAPITAL characters in a word
   * 
   * @params $words
   * @return 
   *  No of Capitals in that word
	 */
  protected function could_capital($words){
    $total_caps =  preg_match_all("/\b[A-Z][A-Za-z]*\b/", $words, $matches);     
    return $total_caps;
  }
  
  /**
   * Match from the list of countries
   * 
   * @params $content
   *  The content to match against
   * @params $vocab
   *  A passed by reference variable to list possible matches
   */
  public function matchcountries($content, &$vocab) {
    $list_countries = variable_get('autordf_list_countries', AUTORDF_LIST_COUNTRIES);
    $list_countries = str_replace(array(', ', ','), '|', $list_countries);

    $tags_countries = &$vocab;
    preg_match_all("/\s($list_countries)[\s.,?]?/i",$content,$matches, PREG_SET_ORDER);
    if($matches){
      foreach($matches as $match) {
        $pat = $match[1];
        if (!$this->keywordexist($pat, $vocab)) {
          $tags_countries[$pat] = isset($tags_countries[$pat])? $tags_countries[$pat]++: 1;
        }
      }
    }
  }
  
  /**
   * Matches content for email addresses
   * 
   * @params $content
   *  The content to match email addresses
   * @params $vocab
   *  A passed by reference variable to list possible matches
   */
  public function matchemails($content, &$vocab) {
    $regexp = "/([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([\w\d-]+)(\.[\w\d-9-]+)*(\.[A-Za-z]{2,4})/";
    $emails = &$vocab;
    preg_match_all($regexp, $content, $matches, PREG_SET_ORDER);
    if($matches) {
      foreach($matches as $match){
        $pat = $match[0];
        $emails[$pat] = isset($emails[$pat])? $emails[$pat]++: 1;
      }
    }
  }

  /**
   * Find out subject object and predicate in a sentence
   */
  public function triplets($content) {
    $subject = $object = $verb = array();
  	return;
  }  
  
  /**
   * Searches the passed in content looking for Acronyms to add to the search tags array
   * 
   * @param $content
   *  Passed in content to match the Acronyms
   * @param $vocab
   *  A passed by reference variable to list possible matches
   */
  public function matchacronyms($content, &$vocab) {
    $searchtags = &$vocab;
    // easiest way to look for keywords without some sort of list is to look for Acronyms like CIA, AIG, JAVA etc.
    // so use a regex to match all words that are pure capitals 2 chars or more to skip over I A etc
    preg_match_all("/\b([A-Z]{2,}[0-9]*)\b/u",$content,$matches,PREG_SET_ORDER);
  
    if($matches) {
    
      foreach($matches as $match) {
        
        $pat = $match[1];

      // ignore noise words who someone has capitalised as well as roman numerals which may be part of something else e.g World War II
        if(!$this->IsRomanNumeral($pat) && !$this->isnoiseword($pat) && !$this->keywordexist($pat, $vocab)) {
          // add in the format key=value to make removing items easy and quick plus we don't waste overhead running
          // array_unique to remove duplicates!         
          $searchtags[$pat] = isset($searchtags[$pat])?$searchtags[$pat]++: 1;
        }
      }
    }
  }
  
  
  /**
   * Searches the content for names
   * 
   * @param $content
   * @param $vocab
   */
  protected function matchnames($content, &$vocab){
    $searchtags = &$vocab['Person'];
    // @TODO: create noise word list

    // look for names of people or important strings of 2+ words that start with capitals e.g Federal Reserve Bank or Barack Hussein Obama
    // this is not perfect and will not handle Irish type surnames O'Hara etc
    @preg_match_all("/((\b[A-Z][^A-Z\s\.,;:]+)(\s+[A-Z][^A-Z\s\.,;:]+)+\b)/u", $content, $matches,PREG_SET_ORDER);

    // found some results
    if($matches){
      foreach($matches as $match){
        $pat = $match[1];
        $searchtags[$pat] = trim($pat);
      }
    }
  }
  
  /**
   * Incomplete 
   */
  protected function matchanniversary($content, &$vocab) {
    $anniversary = &$vocab['Anniversary'];
    $id = "[A-Z]+[a-z]*\s";
    preg_match_all("/(($id)*\s[0-9]{1,3}\s?[st|nd|rd|th]\s($id)*)/", $content, $matches, PREG_SET_ORDER);
  }

  
  public function hidden_markov_model() {
    // Mark the transition of words
    // And make sure the word from which we start and after word is not a stopword
    $text = strip_tags($this->text);
    preg_match_all("/[\w\d\.]+/", $text, $matches);
    $qualify = array();
    $previous_token = '';
    $tokens = array_map('strtolower', $matches[0]);
    foreach($tokens as $token) {
      // remove trailing full stops
      if(substr($token, -1) == '.') {
        $token = preg_replace('/\.+$/', '', $token);
      }
      if (!in_array($token, $this->noisewords) && drupal_strlen($token) >= $this->min_word_length && !preg_match('/[\d\.]+/', $token)) {
        if($previous_token != '') {
          if (isset($qualify[$previous_token][$token])) {
            $qualify[$previous_token][$token]++;
          }
          else {
            $qualify[$previous_token][$token] = 1;
          }
        }
        $previous_token = $token;
      }
      else {
        $previous_token = '';
      }
    }   
    $tokens = $qualify;
    $return = array();
    foreach ($tokens as $token => $values) {
      foreach($values as $key => $value) {
        if($tokens[$token][$key] >= 3)
          $return[] = "$token $key";
      }
    }
    return $return;
  }   
}