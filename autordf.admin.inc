<?php

function autordf_admin_vocab() {
  
  $form['autordf_list_countries'] = array(
    '#type' => 'textarea',
    '#title' => ('List of Countries'),
    '#row' => 4,
    '#default_value' => variable_get('autordf_list_countries', AUTORDF_LIST_COUNTRIES),
  );	
  return system_settings_form($form);    
}

function autordf_admin_settings() {
  $status = '<p>'. t('You may have to rebuild the tag cloud to make Ignore list of tags.') .'</p>';
  $status .= '<p>'. t('Rebuilding may take some time if there is a lot of content.') .'</p>';

  $form['stopwords'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stop words'),
  );  
  $form['stopwords']['status'] = array('#markup' => $status);
  $form['stopwords']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild ignore list'),
    '#submit' => array('autordf_configure_tag_submit'),
  );  

  $form['stopwords']['autordf_stopword_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Threshold'),
    '#default_value' => variable_get('autordf_stopword_threshold', 0.3),
    '#description' => t('Set the threshold for finding stopwords. Must be between 0 and 1. A value of 1 ignore those words that appear on every nodepage and 0 means ignore every word. Ideally it should be between 0.2 - 0.7'),
  );

  $form['stopwords']['autordf_ignorelist_default'] = array(
    '#type' => 'textarea',
    '#title' => t('Default Ignore List'),
    '#description' => t('Default ignore list'),
    '#row' => 4,
    '#default_value' => variable_get('autordf_ignorelist_default', AUTORDF_IGNORE_LIST_DEFAULT),
  );
   
  $form['stopwords']['autordf_ignorelist'] = array(
    '#type' => 'textarea',
    '#title' => t('Generated Ignore List'),
    '#description' => t('Commonly occuring ignore word list'),
    '#row' => 6,
    '#disabled' => TRUE,
    '#default_value' => variable_get('autordf_ignorelist', AUTORDF_IGNORE_LIST_DEFAULT),
  );

  $form['autordf_nestedtags'] = array(
    '#type' => 'radios',
    '#title' => t('Nested Tags'),
    '#options' => array(
      AUTORDF_TAG_ALL => 'Tag All',
      AUTORDF_TAG_MIN => 'Tag Min', 
      AUTORDF_TAG_MAX => 'Tag Max'
    ),
    '#default_value' => variable_get('autordf_nested_tags', 0),
    '#description' => t('This settings determine how relative similar tags gets handled e.g Apple, Apple Iphone, Apple Iphone OS all have "apple". In "Tag All" all 3 will get tagged. In "Tag Max" only Apple Iphone OS will get tagged. In "Tag Min" only Apple gets tagged'),
  );  
    
  $form['autordf_minlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum word length to tag'),
    '#size' => 5,
    '#default_value' => variable_get('autordf_minlength', 3),
  );  
  
  $options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name ;
  }
  $form['autordf_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#options' => $options,
    '#default_value' => variable_get('autordf_vocabulary', 1),
  );

  $node_types = node_type_get_types();
  foreach($node_types as $node_type) {
  	$types[$node_type->type] = $node_type->name;
  }
  
  $form['autordf_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#options' => $types,
    '#default_value' => variable_get('autordf_node_types', array('article')),
  );
  
  
//  debug(array_keys(field_info_instances($entity_type, $bundle)));
  return system_settings_form($form); 
}

function autordf_node_keywords($node) {
  drupal_set_title(t('Terms for: @title', array('@title' => $node->title)), PASS_THROUGH);
  return drupal_get_form('autordf_nodetag_form', $node);
}

function autordf_nodetag_form($form, &$form_state, $node) {
  $vocabulary = variable_get('autordf_vocabulary_names', FALSE);
  $keywords = autordf_get_node_keywords($node->nid);
  if ($keywords) {
    $tags = array();
    foreach ($keywords as $keyword) {
    	$tags[$keyword->vid][] = $keyword->name;
    }
  }
  $form = array();
  
  $form['#tree'] = TRUE;
  
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  
  foreach ($vocabulary as $name => $vid) {
    $value['name'] = autordf_make_readable($name);
    $value['default'] = '';
    if(isset($tags[$vid])) {
      $value['default'] = implode(', ', $tags[$vid]);
    }
    
  	$form['vocabulary'][] = autordf_node_vocabulary_form($vid, $value);
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function autordf_node_vocabulary_form($vid, $value = array()) {
  $form['vid'] = array(
    '#type' => 'value',
    '#value' => $vid,
  );
  
  $description = $value['default']? t('Suggestions: ').$value['default']: '';
  
  $form['tags'] = array(
    '#type' => 'textfield',
    '#title' => $value['name'],
    '#default_value' => $value['default'],
    '#description' => $description,
    '#autocomplete_path' => 'autordf/autocomplete/'. $vid,
//    '#element_validator' => array('autordf_autocomplete_validate'),    
  );

  return $form;	
}

function autordf_autocomplete_validate($element, &$form_state) {
  // Save New Taxonomy terms here
  // Also Validate the term to RDF also exist in the document
}

function autordf_nodetag_form_submit($form, &$form_state) {
	// Complete the Submit callback
}

function autordf_configure_tag_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/autordf/rebuild';
}


function autordf_configure_rebuild_confirm() {
  return confirm_form(array(), t('Are you sure you want to rebuild the stopwords?'),
         'admin/config/autordf/settings', t('This action rebuilds stop words, and may be a lengthy process.'), t('Rebuild tags'), t('Cancel'));
}

function autordf_configure_rebuild_confirm_submit($form, &$form_state) {
  autordf_stopword_rebuild();
  $form_state['redirect'] = 'admin/config/autordf/settings';
}
